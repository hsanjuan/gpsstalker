GPS stalker
===========

[The Seeeduino Stalker v3](http://www.seeedstudio.com/wiki/Seeeduino-Stalker_v3) is an Arduino-compatible board designed for outdoor data-logging applications.

A [GPS Bee kit](http://www.seeedstudio.com/wiki/GPS_Bee_kit) can be plugged to it.

This sketch uses the Stalker to track GPS locations regularly and write the location information to the SD card.

The code is inspired by the [StalkerV30 example](http://www.seeedstudio.com/wiki/File:StalkerV30_DataLogger_10Sec.zip) and uses the `TinyGPSPlus` and `DS1337` libraries (included in `vendor/` for convinience).
