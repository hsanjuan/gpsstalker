/***
  Copyright 2016. Hector Sanjuan.
***/

// Sketch for Seeduino Stalker v3.0 with a GPS Bee module.
// It logs GPS coordinates to the gps.csv file and GPS Bee info to debug.log
// It will poweroff during intervals until waken up by the RTC clock
// by means of an interrupt so it should be pretty battery efficient
//
// Notes:
// * This is heavily based on:
//   www.seeedstudio.com/wiki/File:StalkerV30_DataLogger_10Sec.zip
// * Uses RTC library from: http://www.seeedstudio.com/wiki/File:DS1337.zip
// * P1, P2 and P3 has been soldered. This enables power management of the Bee,
//   of the SD card and makes INT0 is available on Digital Pin D2.   
//   (http://www.seeedstudio.com/wiki/Seeeduino-Stalker_v3#PCB_Jumper_Pads)
// * Board should be set to "Arduino Pro or Pro Mini (3.3V, 8Mhz) w/ ATmega328"
// * GPS Module uses the UART ports so cannot get UARTSBee
//   debugging while in operation. As alternative, a software serial is
//   set up using pins DEBUG_RX and DEBUG_TX. This can be read and relayed to a computer
//   using a different Arduino (remember to wire GND between both!)


#include <avr/sleep.h>
#include <avr/power.h>
#include <avr/power.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <SoftwareSerial.h>
#include "DS1337.h"
#include "TinyGPS++.h"

#define DEBUG_RX 6
#define DEBUG_TX 7
#define SD_POWER 4
#define BEE_POWER 5
#define GPS_SERIAL_SPEED 9600

#define DEBUG_SERIAL //comment to disable software serial logging
#define DEBUG_SERIAL_SPEED 9600
#define DEBUG_INTERVAL 5 // in seconds

// #define SD_CLEANUP // uncomment to remove files from the SD card on startup!!

DS1337 RTC; //Create RTC object for DS1337 RTC
TinyGPSPlus gps;
#ifdef DEBUG_SERIAL
SoftwareSerial debug(DEBUG_RX, DEBUG_TX); // RX, TX
#endif

const uint16_t interruptInterval = 60 * 60; //Seconds. Change this to suitable value.
const uint16_t fixGiveUp = 15 * 60; // If we don't fix in this time (seconds), go to sleep.
static long lastDebug=0;
static long fixStart=0;
static DateTime interruptTime;
static uint32_t gpsFixes = 0;



void setup ()
{
     /*Initialize INT0 pin for accepting interrupts */
     PORTD |= 0x04;
     DDRD &=~ 0x04;

     Wire.begin();
     Serial.begin(GPS_SERIAL_SPEED);      // HW Serial to read GPS Bee module
#ifdef DEBUG_SERIAL
     pinMode(DEBUG_RX, INPUT);
     pinMode(DEBUG_TX, OUTPUT);
     debug.begin(DEBUG_SERIAL_SPEED);       // SW Serial for debugging
#endif
     RTC.begin();

     pinMode(SD_POWER, OUTPUT);   //SD Card power control pin. LOW = On, HIGH = Off
     digitalWrite(SD_POWER, LOW); //Power On SD Card.
     pinMode(BEE_POWER, OUTPUT);
     digitalWrite(BEE_POWER, LOW); //Power On Bee module

     // Check if SD card can be intialized.
     if (!SD.begin(10))  //Chipselect is on pin 10
     {
         logSerial("SD Card could not be intialized, or not found");
     }
     logSerial("SD Card found and initialized.");
     sd_cleanup();


     attachInterrupt(0, INT0_ISR, LOW); //Only LOW level interrupt can wake up from PWR_DOWN
     set_sleep_mode(SLEEP_MODE_PWR_DOWN);

     //Enable Interrupts Every...
     //RTC.enableInterrupts(EveryMinute); //interrupt at  EverySecond, EveryMinute, EveryHour
}

void sd_cleanup()
{
#ifdef SD_CLEANUP
    SD.remove("gps.log");
    SD.remove("debug.log");
    logSerial("SD cleaned up");
#endif
}

void logSerial(char* line)
{
#ifdef DEBUG_SERIAL
    debug.println(line);
#endif
}

// Pads numbers < 10 with a 0. Returns the number to it can be re-used.
// (Ugly function with side effects but we save space by not having to declare
// variables to temporally store RTC values)
uint8_t printPadRTC(Print* p, uint8_t data)
{
    if (data < 10) {
        p->print('0');
    }
    return data;
}

// Print gps info line
// Example: 20160129_23:03:24,52.505714,13.301767,41.20,20160129_23032000
void printGPSInfo(Print* p)
{
    DateTime now = RTC.now();
    p->print(now.year(), DEC);
    p->print(printPadRTC(p, now.month()), DEC);
    p->print(printPadRTC(p, now.date()), DEC);
    p->print('_');
    p->print(printPadRTC(p, now.hour()), DEC);
    p->print(':');
    p->print(printPadRTC(p, now.minute()), DEC);
    p->print(':');
    p->print(printPadRTC(p, now.second()), DEC);
    p->print(',');
    p->print(gps.location.lat(), 6);
    p->print(",");
    p->print(gps.location.lng(), 6);
    p->print(",");
    p->print(gps.altitude.meters());
    p->print(",");
    if (gps.date.isValid()){
        p->print(gps.date.value());
    } else {
        p->print("Invalid");
    }
    p->print("_");
    if (gps.time.isValid()){
        p->print(gps.time.value());
    } else {
        p->print("Invalid");
    }
    p->println();

}

// Print gps debug line
// Example: 
// 20160129_23:27:28 | Chars=0 | Passed=0 | Failed=0 | Fix=0 | VS=0 | Age=4294967295 | Date_Time:Invalid_Invalid
void printGPSDebug(Print* p)
{
        DateTime now = RTC.now();
        p->print(now.year(), DEC);
        p->print(printPadRTC(p, now.month()), DEC);
        p->print(printPadRTC(p, now.date()), DEC);
        p->print('_');
        p->print(printPadRTC(p, now.hour()), DEC);
        p->print(':');
        p->print(printPadRTC(p, now.minute()), DEC);
        p->print(':');
        p->print(printPadRTC(p, now.second()), DEC);
        p->print(" | Chars=");
        p->print(gps.charsProcessed());
        p->print(" | Passed=");
        p->print(gps.passedChecksum());
        p->print(" | Failed=");
        p->print(gps.failedChecksum());
        p->print(" | Fix=");
        p->print(gps.sentencesWithFix());
        p->print(" | VS=");
        p->print(gps.satellites.value());
        p->print(" | Age=");
        p->print(gps.location.age());
        p->print(" | Date_Time:");
        if (gps.date.isValid()){
            p->print(gps.date.value());
        } else {
            p->print("Invalid");
        }
        p->print("_");
        if (gps.time.isValid()){
            p->print(gps.time.value());
        } else {
            p->print("Invalid");
        }
        p->println();

}

// Power off routine. Turns off everything. Can only wake up by RTC INT.
void powerOff()
{
    //Power Down routines
    cli();
    sleep_enable();      // Set sleep enable bit
    sleep_bod_disable(); // Disable brown out detection during sleep. Saves more power
    sei();

    digitalWrite(SD_POWER, HIGH); //Power Off SD Card.
    digitalWrite(BEE_POWER, HIGH); //Power Off Bee Module.

    logSerial("Sleeping...");
    delay(10); //This delay is required to allow print to complete
    //Shut down all peripherals like ADC before sleep. Refer Atmega328 manual
    power_all_disable(); // This shuts down ADC, TWI, SPI, Timers and USART
    sleep_cpu();         // Sleep the CPU as per the mode set earlier(power down)
}

// Wake up routine. Should happen after poweroff, usually because we have been woken by an INT.
void wakeUp()
{
    sleep_disable();     // Wakes up sleep and clears enable bit. 
    power_all_enable();  // This enables ADC, TWI, SPI, Timers and USART
    delay(10); //This delay is required to allow CPU to stabilize
    logSerial(" awake!");
    digitalWrite(SD_POWER, LOW); //Power On SD Card.
    digitalWrite(BEE_POWER, LOW); //Power on Bee Module.
}

// Writes the GPS information line to gps.log
void logInfo()
{
    File logFile = SD.open("gps.log", FILE_WRITE);
    if(logFile) {
        printGPSInfo(&logFile);
        logFile.close();
    } else {
        logSerial("Could not write to gps.log");
    }
}

// Print the GPS information to the debug SW serial 
void printInfo()
{
#ifdef DEBUG_SERIAL
    printGPSInfo(&debug);
#endif
}

// Writes the GPS debug line to debug.log
void logDebug()
{
    File debugFile = SD.open("debug.log", FILE_WRITE);
    if(debugFile) {
        printGPSDebug(&debugFile);
        debugFile.close();
    } else {
        logSerial("Could not write to debug.log");
    }
}

// Print the GPS debug line to the debug SW serial 
void printDebug()
{
#ifdef DEBUG_SERIAL
    printGPSDebug(&debug);
#endif
}

void loop ()
{
    /*
    float voltage;
    int BatteryValue;
    BatteryValue = analogRead(A7);
    voltage = BatteryValue * (1.1 / 1024)* (10+2)/2;  //Voltage divider
    */

    
    // While we have an invalid or aged location information, query until we get 5 proper fixes
    fixStart = RTC.now().get();
    while (!gps.location.isValid() || (gps.sentencesWithFix() - gpsFixes) <= 5) {
        DateTime now = RTC.now(); //get the current date-time
        while (Serial.available() > 0) {
            gps.encode(Serial.read());
        }
        if((now.get() - lastDebug) > DEBUG_INTERVAL ) //print only in DEBUG_INTERVAL
        {
            printDebug();
            logDebug();
            lastDebug = now.get();
        }
        if ((now.get() - fixStart) > fixGiveUp)
        {
            logSerial("Fixing is taking too long. Giving up...");
            break;
        }
    }

    // Log this nice and valid GPS Fix!!
    if ((gps.sentencesWithFix() - gpsFixes) > 5)
    {
        gpsFixes = gps.sentencesWithFix();
        printDebug();
        logDebug();
        printInfo();
        logInfo();
    }

    //This function call is a must to bring /INT pin HIGH after an interrupt.
    RTC.clearINTStatus();
    // decide the time for the next interrupt, configure next interrupt
    interruptTime = DateTime(RTC.now().get() + interruptInterval);
    // set the interrupt at (h,m,s)
    RTC.enableInterrupts(interruptTime.hour(),interruptTime.minute(),interruptTime.second());
    // Enable INT0 interrupt (as ISR disables interrupt).
    // This strategy is required to handle LEVEL triggered interrupt
    attachInterrupt(0, INT0_ISR, LOW);


    powerOff();
    //ISR happens here
    wakeUp();

}

//Interrupt service routine for external interrupt on INT0 pin conntected to DS1337 /INT
void INT0_ISR()
{
    //Keep this as short as possible. Possibly avoid using function calls
    detachInterrupt(0);
}
